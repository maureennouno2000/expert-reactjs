import React, { Component } from 'react'

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date().toLocaleDateString().split('/').reverse().join('-'),
      hour:'02:30'
    }
  }

  render() {
    return (
      <div className='appBody updateForm' id= "updateForm">
      
 
          <label for="date">Date : </label>
        <input value={this.state.date} onChange={(e) => this.setState({
          ...this.state,
          date:e.target.valueAsDate.toLocaleDateString().split('/').reverse().join('-')
        })}
          type='date' id="date" /><br />
          <label for="time">Heure : </label>
        <input type='time' value={this.state.hour} id="time"
          onChange={(e) => this.setState({
            ...this.state,
            hour:e.target.value
          })} />
        <br />   
          <button  onClick={() =>this.props.onChangeDate(new Date(this.state.date+' '+this.state.hour))}>Modifier</button>
      
      </div>
    )
  }
}
