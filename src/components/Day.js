import React from 'react'

export default function Day(props) {
    const Day=['Dimanche','Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

    return (
        <p className='xs'>
            {Day[props.date.getDay()].toUpperCase()}
        </p>
    )
}
