import React from "react";
import Form from "./Form";
import Day from "./Day";
import Time from "./Time";
import DisplayDate from "./DisplayDate";

function moment(date) {
  if (date >= 19 || date < 6) {
    return "Nuit";
  } else if (date < 12) {
    return "Matin";
  } else if (date == 12) {
    return "Midi";
  } else if (date < 16) {
    return "Après-midi";
  } else {
    return "Soir";
  }
}

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      update: false,
    };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    const { date } = this.state
      const nextDate =  new Date(date.getTime() + 1000);

    this.setState((prevState) => ({
        ...prevState,
        date: nextDate
      }))
  }




  render() {
    return (
      <React.Fragment>
        <h1 className="appTitle">Horloge Numérique by MDN</h1>
        {this.state.update ? (
          <Form onChangeDate={(date) => 
            this.setState({
            ...this.state,
              update: false,
              date
          })} />
        ) : (
          <div className="appBody" id="clock">
            <Day date={this.state.date} />
            <p className="sm">
              {moment(this.state.date.getHours()).toUpperCase()}
            </p>
            <Time date={this.state.date} />
            <DisplayDate date={this.state.date} />
            <button
              onClick={() =>
                this.setState((prevState) => ({
                  ...prevState,
                  update: true,
                }))
              }
            >
              Modifier
            </button>
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default Clock;
