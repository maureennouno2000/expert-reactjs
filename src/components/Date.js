import React from 'react'

export default function DisplayDate(props) {

    const Month=['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    return (
        <p className='sm'>
            {props.date.getDate() + ' ' + Month[props.date.getMonth()].toUpperCase() + ' ' + props.date.getFullYear() }
        </p>
    )
}

